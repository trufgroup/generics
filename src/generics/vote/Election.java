package generics.vote;

public interface Election {

    ElectionResults getResults();
}
