package generics.map;

import java.util.Map;

public interface CounterMap {

    void add(Object obj);

    int getCount(Object obj);

    int remove(Object obj);

    int size();

    //Добавить все элементы из anotherMap в текущий контейнер, при совпадении ключей - суммировать значения
    void addAll(CounterMap anotherMap);

    //Вернуть java.util.Map. ключ - добавленный элемент, значение - количество его добавлений
    Map toMap();

    //Тот же самый контракт как и toMap(), только всю информацию записать в map
    void toMap(Map map);
}
